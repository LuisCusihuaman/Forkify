# How It Works " **[Forkify](https://luiscusihuaman.github.io/Forkify/)**"

This is a Kitchen Recipe Finder, MVC project  to learn the advanced of Javascript ES6, 7, 8, 9 features .  This is an app that searches for recipes, modify servings and add to the shopping list & favorite recipes.

Based  on the projects of the Complete JavaScript Course.

# What I Learned

*Next Generation JavaScript:  ECMAScript 6*
- Variable Declarations with let and const
- Blocks and IIFEs
- Strings in ES6 / ES2015
- Arrow Functions
- Destructuring
- Arrays in ES6 / ES2015
- The Spread Operator
- Rest Parameters
- Default Parameters
- Maps
- Classes with new syntax

*Asynchronous JavaScript:  ECMAScript 7, 8, 9* 
- Array.prototype.includes()
- Exponentiation operator
- Understanding Asynchronous JavaScript: The Event Loop
- Connect with APIs
- Promises
- From Promises to Async/Await
- Making AJAX Calls with Fetch and Promises
- Making AJAX Calls with Fetch and Async/Await
- Modern Js: NPM, Babel and Configuring The Webpack Dev Server
- Modules
- Implementing Persistent Data with localStorage
